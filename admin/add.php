<?php
    include 'database.class.php';
    include 'book.class.php';
?>
    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>BookZilla</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    </head>
<body>
<header>
    <div class="navbar navbar-dark bg-dark shadow-sm">
        <div class="container d-flex justify-content-between">
            <a href="index.php" class="navbar-brand d-flex align-items-center">
                <img src="https://img.icons8.com/cute-clipart/64/000000/book.png">
                <h1>BookZilla</h1>
            </a>
        </div>
    </div>
    <section class="card-body text-center">
        <div class="container">
            <h1>BookZilla</h1>
            <p class="lead text-muted">Пришёл. Увидел. Купил.</p>
        </div>
    </section>
</header>

    <main role="main">
        <div class="container">
            <div class="row">
                <?php include 'aside.php' ?>
                <div class="col-md-9">
                    <form action="" method="post">
                        <h4>Внесите данные для добавления в БД</h4>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Название</span>
                            </div>
                            <input type="text" class="form-control" name="title" aria-label="title" required><!--title-->
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Автор</span>
                            </div>
                            <input type="text" class="form-control" name="author" aria-label="author" required><!--author-->
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Описание</span>
                            </div>
                            <textarea name="description" cols="30" rows="5" class="form-control"></textarea><!--description-->
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Жанр</span>
                            </div>
                            <input type="text" class="form-control" name="genre" aria-label="genre" required><!--genre-->
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Цена</span>
                            </div>
                            <input type="text" class="form-control" name="price" aria-label="price" required><!--price-->
                        </div>
                        <div class="input-group mb-3">
                            <button name="add_book" type="submit" class="btn btn-outline-success">Добавить</button>
                        </div>
                    </form>
                    <?php
                        if(isset($_POST['add_book']))
                        {
                            $data = [
                                'title' => $_POST['title'],
                                'author' => $_POST['author'],
                                'description' => $_POST['description'],
                                'genre' => $_POST['genre'],
                                'price' => $_POST['price'],
                            ];
                            $addBook = new Book();
                            $addBook->addBook($data);
                        }
                    ?>
                </div>
            </div>
        </div>
    </main>
<footer class="container-fluid text-muted">
    <p class="text-right">
        <a href="#"><img src="https://img.icons8.com/cute-clipart/64/000000/circled-up.png"></a>
    </p>
    <p class="text-center">
        BookZilla™
        <?php echo date('Y')?>
    </p>
</footer>
</body>
</html>
