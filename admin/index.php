<?php
    include 'database.class.php';
    include 'book.class.php';
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>BookZilla</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<header>
    <div class="navbar navbar-dark bg-dark shadow-sm">
        <div class="container d-flex justify-content-between">
            <a href="index.php" class="navbar-brand d-flex align-items-center">
                <img src="https://img.icons8.com/cute-clipart/64/000000/book.png">
                <h1>BookZilla</h1>
            </a>

        </div>
    </div>
    <section class="card-body text-center">
        <div class="container">
            <h1>BookZilla</h1>
            <p class="lead text-muted">Пришёл. Увидел. Купил.</p>
        </div>
    </section>
</header>
<main role="main">
    <div class="container">
        <div class="row">
            <?php include './aside.php' ?>
            <div class="col-md-9">
                <h4>Библиотека</h4>
                <?php
                    $books = new Book();
                    foreach ($books->getBooks() as $book) :
                ?>
                <div class="col-md-12 card mb-3">
                    <div class="card-body">
                        <p><b>Наименование: </b><span><?= $book['title'] ?></span></p>
                        <p><b>Автор: </b><span><?= $book['author'] ?></span></p>
                        <p><b>Жанр: </b><span><?= $book['category'] ?></span></p>
                        <p><b>Описание: </b><span><?= $book['description'] ?></span></p>
                        <p><b>Цена: </b><span><?= $book['price'] ?></span></p>
                        <p><b>ID: </b><span><?= $book['id'] ?></span></p>
                        <a href="delete.php?id=<?=$book['id']?>" class="btn btn-outline-danger">Удалить</a>
                    </div>
                </div>
                <?php
                    endforeach;
                ?>
            </div>
        </div>
    </div>
</main>
<footer class="container-fluid text-muted">
    <p class="text-right">
        <a href="#"><img src="https://img.icons8.com/cute-clipart/64/000000/circled-up.png"></a>
    </p>
    <p class="text-center">
        BookZilla™
        <?php echo date('Y')?>
    </p>
</footer>
</body>
</html>
