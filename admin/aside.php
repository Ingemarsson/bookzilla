<div class="col-md-3">
    <div class="fading-side-menu affix-top">
        <h5>Admin</h5>
        <hr class="no-margin">
        <ul class="list-unstyled">
            <li>
                <a href="index.php">
                    <span class="fa fa-angle-double-right text-primary"></span>Главная
                </a>
            </li>
            <li>
                <a href="./add.php">
                    <span class="fa fa-angle-double-right text-primary"></span>Добавить Книгу
                </a>
            </li>
            <li>
                <a href="mails.php">
                    <span class="fa fa-angle-double-right text-primary"></span>Просмотр писем
                </a>
            </li>
            <li>
                <a href="../index.php">
                    <span class="fa fa-angle-double-right text-primary"></span>На сайт
                </a>
            </li>
        </ul>
    </div>
</div>