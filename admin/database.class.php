<?php

class Database
{
    private $host = 'localhost';
    private $login = 'root';
    private $password = 'root';
    private $db_name = 'books';
    private $connection;

    private function connect() {
        $dsn = 'mysql:host='.$this->host.';dbname='.$this->db_name;
        $this->connection = new PDO($dsn, $this->login, $this->password);        
        $this->connection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

        return $this->connection;
    }

    protected function getConnection()
    {
        if(!$this->connection) {
            $this->connect();
        }

        return $this->connection;
    }
}
