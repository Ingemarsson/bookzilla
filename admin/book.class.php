<?php

class Book extends Database
{
    public function getBooks() {
        $sql = 'SELECT * FROM book';
        $stmt = $this->getConnection()->query($sql);
        return $stmt->fetchAll();
    }
    public function getBook($id) {
        $sql = "SELECT id, title, author, description, price, category FROM book WHERE id = $id";
        $stmt = $this->getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
    public function addBook ($data) {
        $sql = "INSERT INTO book(title, author, description, price, category) 
VALUES ('".$data['title']."', '".$data['author']."', '".$data['description']."', '".$data['price']."', '".$data['category']."')";
        $stmt = $this->getConnection()->prepare($sql);
        $stmt->execute();
        return true;
    }
    public function searchBook($query) {
        $sql = "SELECT * FROM book WHERE title LIKE '$query'";
        $stmt = $this->getConnection()->prepare($sql);
        //var_dump($stmt);
        $stmt->execute();
    }
    public function deleteBook($id) {
        $sql = "DELETE FROM book WHERE id = $id";
        $stmt = $this->getConnection()->prepare($sql);
        $stmt->execute();
    }
}