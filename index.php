<?php
    include './header.php';
    include './admin/database.class.php';
    include './admin/book.class.php';
?>
    <main role="main">
<!--        <div class="album">-->
<!--            <div class="container">-->
<!--                <form class="row align-items-center justify-content-center mb-5" action="" method="get">-->
<!--                    <div class="col-md-6 pt-3">-->
<!--                        <div class="input-group mb-3">-->
<!--                            <div class="input-group-prepend">-->
<!--                                <span class="input-group-text">Введите автора или жанр</span>-->
<!--                            </div>-->
<!--                            <input type="text" class="form-control" name="search_query" aria-label="search">-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="col-md-2">-->
<!--                        <button type="submit" class="btn btn-success btn-block" name="search">Поиск</button>-->
<!--                    </div>-->
<!--                </form>-->
<!--            </div>-->
            <div class="container">
                <div class="row">
                    <?php
                        $books = new Book();
                        foreach ($books->getBooks() as $book) :
                    ?>
                    <div class="col-md-3">
                        <div class="card mb-4 shadow-sm">
                            <img src="http://via.placeholder.com/255x180" />
                            <div class="card-body text-center"><span class="badge badge-warning"><?=
                            $book['category'] ?></span>
                                <h6 class="mt-2"><?= $book['title'] ?></h6>
                                <p><?= $book['author'] ?></p>
                                <div class="d-flex justify-content-center">
                                    <a href="book.php?id=<?= $book['id'] ?>" class="btn btn-primary">Подробнее</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </main>
<?php include_once './footer.php' ?>