<footer class="container-fluid text-muted">
    <p class="text-right">
        <a href="#"><img src="https://img.icons8.com/cute-clipart/64/000000/circled-up.png"></a>
    </p>
    <p class="text-center">
        BookZilla™
        <?php echo date('Y')?>
    </p>
</footer>
</body>
</html>
