<?php
include "./admin/database.class.php";
include "./admin/book.class.php";
?>
    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>BookZilla</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    </head>
<body>
<header>
    <div class="navbar navbar-dark bg-dark shadow-sm">
        <div class="container d-flex justify-content-between">
            <a href="index.php" class="navbar-brand d-flex align-items-center">
                <img src="https://img.icons8.com/cute-clipart/64/000000/book.png">
                <h1>BookZilla</h1>
            </a>
        </div>
    </div>
    <section class="card-body text-center">
        <div class="container">
            <h1>BookZilla</h1>
            <p class="lead text-muted">Пришёл. Увидел. Купил.</p>
        </div>
    </section>
</header>
    <main role="main">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <img src="http://via.placeholder.com/348x450" alt="">
                </div>
                <div class="col-md-8">
                    <div class="card-body">
                        <?php
                            $book = new Book();
                            $id = $_GET['id'];
                            $bookInfo = $book->getBook($id);
                        ?>
                        <span class="badge badge-warning"><?= $bookInfo['category']?></span>
                        <h4><?= $bookInfo['title'] ?></h4>
                        <h5 class="text-muted"><?= $bookInfo['author'] ?></h5>
                        <p><?= $bookInfo['description'] ?></p>
                        <div class="row no-gutters">
                            <div class="col-md-3 mt-1">
                                <h5>Цена: <span class="text-success"><?= $bookInfo['price'] ?>$</span></h5>
                            </div>
                            <div class="col-md-3">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Кол-во</span>
                                    </div>
                                    <input type="number" name="counter" class="form-control input-number" value="1" min="1" max="10">
                                </div>
                            </div>
                        </div>
                        <form action="" method="post">
                            <div class="form-group">
                                <label for="name">ФИО</label>
                                <input type="text" class="form-control mb-3" name="name" placeholder="Маннергейм" required>
                                <input type="text" class="form-control mb-3" name="surname" placeholder="Христофор" required>
                                <input type="text" class="form-control mb-3" name="secondname"
                                       placeholder="Автандилович" required>
                            </div>
                            <div class="form-group">
                                <label for="adress" >Адрес</label>
                                <input type="text" class="form-control" name="adress" placeholder="г. Николаев, ул.
                                Плюшкина 38, кв. 24" required>
                            </div>
                            <div class="form-group">
                                <label for="phone">Телефон</label>
                                <input type="tel" class="form-control" name="phone" placeholder="+380666666666" required>
                            </div>
                            <button type="submit" class="btn btn-primary" name="sendEmail">Купить</button>
                        </form>
                    </div>
                    <?php
                        if(isset($_POST['sendEmail']))
                        {
                            $data = [
                                'name' => $_POST['name'],
                                'surname' => $_POST['surname'],
                                'secondname' => $_POST['secondname'],
                                'adress' => $_POST['adress'],
                                'phone' => $_POST['phone'],
                            ];
                            $emails = fopen('./mail.txt', 'a+');
                            fputs($emails, "\n<p><b>ФИО:</b> <span>".$data['name']." ".$data['surname']." ".$data['secondname']."</span></p><p><b>Адрес:</b> <span>".$data['adress']."</span></p><p><b>Телефон:</b> <span>".$data['phone']."</span></p><hr>");
                            fclose($emails);
                        }
                    ?>
                </div>
            </div>
        </div>
    </main>
<footer class="container-fluid text-muted">
    <p class="text-right">
        <a href="#"><img src="https://img.icons8.com/cute-clipart/64/000000/circled-up.png"></a>
    </p>
    <p class="text-center">
        BookZilla™
        <?php echo date('Y')?>
    </p>
</footer>
</body>
</html>
